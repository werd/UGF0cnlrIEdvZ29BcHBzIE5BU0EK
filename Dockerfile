from golang:1.19.1

WORKDIR /app
COPY go.mod go.sum *.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .


from alpine:latest

RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=0 /app/main .

CMD ["./main"]
