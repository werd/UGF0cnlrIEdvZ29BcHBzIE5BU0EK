package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fastjson"
)

type channelMessage struct {
	dataSlice *[]string
	date      string
	wgURL     *sync.WaitGroup
	mtxLock   *sync.Mutex
}

var (
	// Port specifies http listen port
	Port string

	// ConcurrentRequests sets maximum concurrent requests to nasa api
	ConcurrentRequests int

	// APIKey sets nasa api key
	APIKey string

	concurrentChannel chan channelMessage
	wg                sync.WaitGroup
)

// APIUrl has base url for nasa apod api
const APIUrl = "https://api.nasa.gov/planetary/apod"

func getImageURL(data channelMessage) {
	url := APIUrl + fmt.Sprintf("?api_key=%s&date=%s", APIKey, data.date)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	resBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Print("Error reading body. ", err)
	}

	var p fastjson.Parser
	b, _ := p.Parse(string(resBody))
	imageURL := string(b.GetStringBytes("url"))

	if imageURL == "" {
		log.Print("Couldn't get image url")
	}

	data.mtxLock.Lock()
	*data.dataSlice = append(*data.dataSlice, imageURL)
	data.mtxLock.Unlock()
	data.wgURL.Done()
}

func makeWorkers() {
	wg.Add(ConcurrentRequests)
	for i := 0; i < ConcurrentRequests; i++ {
		go func() {
			for {
				message, ok := <-concurrentChannel
				if !ok {
					wg.Done()
					return
				}
				getImageURL(message)
			}
		}()
	}
}

func picturesHandler(c *fiber.Ctx) error {
	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	if startDate == "" || endDate == "" {
		c.Status(http.StatusBadRequest)
		return c.JSON(map[string]interface{}{"error": "Missing date parameter"})
	}

	timeStartDate, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return c.JSON(map[string]interface{}{"error": "Cannot parse date"})
	}
	timeEndDate, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return c.JSON(map[string]interface{}{"error": "Cannot parse date"})
	}
	if timeEndDate.Before(timeStartDate) {
		c.Status(http.StatusBadRequest)
		return c.JSON(map[string]interface{}{"error": "End date before start date"})
	}

	var returnDates []string
	var wgURL sync.WaitGroup
	var mtxLock sync.Mutex

	for timeStartDate.Before(timeEndDate) || timeStartDate.Equal(timeEndDate) {
		getDateURL := timeStartDate.Format("2006-01-02")

		wgURL.Add(1)
		concurrentChannel <- channelMessage{&returnDates, getDateURL, &wgURL, &mtxLock}
		timeStartDate = timeStartDate.AddDate(0, 0, 1)
	}

	wgURL.Wait()
	log.Println(returnDates)

	return c.JSON(fiber.Map{"urls": returnDates})
}

// SetEnvs sets environment variables, defaults are for development
func SetEnvs() {
	var ok bool

	Port, ok = os.LookupEnv("PORT")
	if !ok {
		Port = "8080"
	}

	concurrentRequestsVar, ok := os.LookupEnv("CONCURRENT_REQUESTS")
	if !ok {
		ConcurrentRequests = 5
	} else {
		var err error
		ConcurrentRequests, err = strconv.Atoi(concurrentRequestsVar)
		if err != nil {
			log.Fatal(err)
		}
	}

	APIKey, ok = os.LookupEnv("API_KEY")
	if !ok {
		APIKey = "DEMO_KEY"
	}
}

func main() {
	SetEnvs()
	makeWorkers()
	concurrentChannel = make(chan channelMessage, ConcurrentRequests)

	app := fiber.New(fiber.Config{
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	})

	app.Get("/pictures", picturesHandler)

	log.Fatal(app.Listen(fmt.Sprintf(":%s", Port)))
}
