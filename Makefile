dockerbuild:
	docker build -t gogo:latest .

rundocker:
	docker run  --rm --name gogo -p 8080:8080 gogo:latest

all: dockerbuild rundocker
