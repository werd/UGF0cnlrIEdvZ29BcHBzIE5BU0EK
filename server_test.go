package main

import (
	"os"
	"testing"
)

func TestDefaultEnvs(t *testing.T) {
	SetEnvs()

	if Port != "8080" {
		t.Log("Default port should be 8080")
		t.Fail()
	}
	if ConcurrentRequests != 5 {
		t.Log("Default concurrency should be 5")
		t.Fail()
	}
	if APIKey != "DEMO_KEY" {
		t.Log("Default api key should be DEMO_KEY")
		t.Fail()
	}
}

func TestNonDefaultEnvs(t *testing.T) {
	os.Setenv("PORT", "80")
	os.Setenv("CONCURRENT_REQUESTS", "10")
	os.Setenv("API_KEY", "test_api_key")

	SetEnvs()

	if Port != "80" {
		t.Log("Non default port should be 80")
		t.Fail()
	}
	if ConcurrentRequests != 10 {
		t.Log("Non default concurrency should be 10")
		t.Fail()
	}
	if APIKey != "test_api_key" {
		t.Log("Non default api key should be test_api_key")
		t.Fail()
	}
}
